# CHATube

A load-balancer for Invidious instances.

### How it works

All requests are redirected with their parameters to a random
instance among those listed in `instances.ts`.

It checks every 30min the instances listed for a test video, 
and remembers their HTTP status. Banned instances typically
return HTTP 500, and will not be the target of a redirection
until a later positive check occurs.

### Stats

The service exposes some statistics at `/status`, as well
as the status code returned by each Invidious instance checked.
