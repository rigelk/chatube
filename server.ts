import * as express from "express"
import { instances as _instances } from "./instances"
import Axios from "axios"
const cron = require('cron').CronJob
const loadbalance = require('loadbalance')
const pRetry = require('p-retry')
import * as pino from "pino"
import * as expressPinoLogger from "express-pino-logger"
var router = express.Router()

export const logger = pino({
  name: 'chatube',
  level: 'debug',
  prettyPrint: true,
  redact: {
    paths: ['req.headers', 'req.url', 'res.headers.location'],
    censor: (str) => typeof str === 'string' && str.startsWith('https://') ? str.split('/').slice(0, 3).join('/') + '/[REDACTED]' : '[REDACTED]'
  }
})

/**
 * The server tests once at startup, and then every 30min, the status
 * returned by a given test page. Invidious typically returns 500 when
 * banned, so we take advantage of that to keep track of instances that
 * are operational.
 * 
 * Not-banned instances are then the only ones redirected to.
 */
const instanceTestPage = "watch?v=DoYXjOk0uys"
let instances = _instances.map(i => { return { host: i, status: 200 }})
let goodInstances = instances
let engine = loadbalance.roundRobin(goodInstances)

let app: express.Application = express()
app.use(require('express-status-monitor')({
  /**
   * Route that shows a summary of performance (req/sec and response time) and instances that are operational (= not banned)
   */
  title: 'CHATube Statistics',
  path: '/status',
  chartVisibility: {
    cpu: false,
    mem: false,
    load: false,
    responseTime: true,
    rps: true,
    statusCodes: false
  },
  healthChecks: instances.map(i => { return {
    protocol: 'http',
    host: 'localhost',
    path: `/instance/${i.host}`,
    port: `${+process.env.PORT || 3000}`
  }}),
  ignoreStartsWith: '/instance'
}))
app.get("/instance/:host", async (req, res) => {
  /**
   * Route that returns the last known status of an instance. Used by the statistics page.
   */
  res.status(instances.find(i => i.host === req.params.host)?.status || 500).end()
})
router.use(expressPinoLogger({ logger }))
router.get("*", (req, res) => {
  /**
   * Route that redirect the user to an instance with a 2xx last known status.
   */
  if (goodInstances.length === 0) res.status(500).send({ error: 'No Invidious instance is currently operational among CHATONS instances. Please have a look at https://instances.invidio.us/ for alternatives.' })
  res.status(307).redirect(`https://${engine.pick().host}${req.originalUrl}`)
})
app.use("*", router)
app.disable('x-powered-by')
app.listen(+process.env.PORT || 3000, () => {
  logger.info(
    `server listening on "http://localhost:${+process.env.PORT || 3000}"`
  )
  new cron('*/30 * * * *', async () => {
    // refresh the known status of an instance every 30min.
    instances = await Promise.all(
      instances.map(async i => {
        return {
          ...i,
          status: (await pRetry(() => Axios.get(`https://${i.host}/${instanceTestPage}`, { timeout: 3000, headers: { 'User-Agent': 'CHATube' } }), { retries: 3 }).catch(err => null))?.status || 500
        }
      })
    )
    goodInstances = instances.filter(i => i.status.toString().startsWith('2') && i.status.toString().length === 3)
    engine = loadbalance.roundRobin(goodInstances)
    instances.forEach(i => {
      i.status.toString().startsWith('2') && i.status.toString().length === 3
        ? logger.info(`instance ${i.host} operational with status ${i.status}`)
        : logger.error(`instance ${i.host} non-operational with status ${i.status}`)
    })
  }, null, null, null, null, true).start() // null values are skipped parameter definitons of the constructor, true is to say "I want the job to run once at startup too"
})
